# JavaScript Project 1

---

# 💻 Manipulation: Number

---

## Requirements

Play around with various kind of number manipulation capabilities in JavaScript.

### Input

* Mostly numbers

### Process

* Addition
* Substraction
* Multiplication
* Division
* Modulation

### Output

* Transformed numbers


---

## Level 0

* Have some transformation to those number variables: addition, substraction, multiplication, division, modulation
* Use basic mathematical operators to accomplish that
* Output the results to the console


Test Case

```js
console.log(add(4,2))
console.log(substract(4,2))
console.log(divide(4,2))
console.log(modulo(4,2))
```

Output

```js
6
2
2
0
```

---

## Level 1

* Try to operate those variables with a different types like string and boolean
* Try to use a negative number or a very big number, then operate on them

Test Case

```js
console.log(add(2,"abc"))
console.log(add(2,true))
console.log(add(2,-999999999999999))
```

Output

```js
Nan
2
-1999999999999998
```

---

## Level 2

* Store different kind of numbers into an array variable
* Combine, push, or pop different kind of data types like a string and boolean to that array variable
* Loop through the numbers or items in the array, then log them to the console

Test Case

```js
console.log(arrayOfNumber)
console.log(pushArray(arrayOfNumber, 6))
console.log(popArray(arrayOfNumber))
console.log(popArray(arrayOfNumber))
console.log(pushArray(arrayOfNumber, 99))
```

Output

```
(5) [1, 2, 3, 4, 5]
(6) [1, 2, 3, 4, 5, 6]
(5) [1, 2, 3, 4, 5]
(4) [1, 2, 3, 4]
(5) [1, 2, 3, 4, 6]
```
---

# 💻 Manipulation: String

---

## Requirements

Play around with various kind of string manipulation capabilities in JavaScript.

### Input

* Mostly texts

### Process

* Concatination
* Upper case
* Lower case
* Title case

### Output

* Transformed texts

---

## Level 3

* Create some simple sentences that stored in different variables
* Those values inside those variables should not be changed
* If you want to create a new value, you should create a new variable
* Log those strings to the console

```js
const sentenceA = "Hello World!";
const sentenceB = "Good Bye!";

console.log(sentenceA);
console.log(sentenceB);
```


---

## Level 4

* Combine those different sentences into one sentence

```js
const sentenceC = sentenceA + " " + sentenceB;
```

* Try to use string template literals to save time

```js
const sentenceD = `${sentenceA} ${sentenceB}`;
```

* Again, log those strings to the console

Test Case

```js
console.log(combineString(sentenceA, sentenceB))
```

Output

```js
Hello World! Good Bye!
```



---

## Level 5

* Transform all the characters of a string into different case of letters: uppercase and lowercase

Test Case:

```js
console.log(displayUpperCase(sentenceA))
console.log(displayLowerCase(sentenceA))
```

Output:

```js
HELLO WORLD!
hello world!
```

---

## Level 6

* Transform all the characters of a string into title case

Test Case:

```js
displayTitleCase("HeLLoW WooRlDD")
displayTitleCase("JAVaScrIPT STRing ManiPulation")

```

Output example:

```js
Hellow Woorldd
Javascript String Manipulation
```

* You are free to change the value of your variables

---

# 💻 Comparison: Number

---

## Requirements

Compare some numbers then determine which one is bigger or smaller.

### Input

* Two or more different numbers

### Process

* Compare number and number

### Output

* Telling which one is bigger or smaller


---

## Level 7

* Store different numbers into some variables
* Create some `if else` statement blocks that tell if each number is bigger or smaller than our determined value
* Log that results to the console

Test Case :
```js
console.log(biggerOrSmaller(1, 2))
console.log(biggerOrSmaller(2, 1))
```

Ouput: 
```js
1 is smaller than 2
2 is bigger than 1
```
---

## Level 8

* Try to compare those numbers with each other
* Expand the `if else` statements into multiple chains

```js
if (condition) {
  // code
} else if (condition) {
  // code
} else {
  // code
}
```

Test Case:

```js
console.log(compareNumber(1, 2))
console.log(compareNumber(2, 1))
console.log(compareNumber(2, 2))
```

Output:

```js
1 is smaller than 2
2 is bigger than 1
2 is equal with 2
```

---

## Level 9

* Use ternary operator `? :` rather than `if else` statement to check the nu

```js
condition ? resultA : resultB;
```

Test Case

```js
console.log(isBigger(2, 1))

```

Output
```js
true
```

---

## Level 10

* Try to compare a variable with value range, with more than 2 conditions, bigger and smaller than some different value at the same time
* Experiment with various kind of operations

```js
min >= condition >= max;
```

```js
conditionA && conditionB;
```

Test Case:

```js
console.log(betweenOneAndTen(5))
console.log(betweenOneAndTen(9))
console.log(betweenOneAndTen(22))
```

Output:

```js
true
true 
false
```

---

## Level 11

* Create a random number generator

```js
console.log(getRandomNumber())
console.log(getRandomNumber())
console.log(getRandomNumber())
```

---

# 💻 Calculator: Math

---

## Requirements

Build a simple calculator to compute basic math operations.

### Input

* Numbers

### Process

* Several functions to run different kind of operations

### Output

* Calculated numbers

---

## Level 12

* Experiment to call some chain of different functions in other functions

```js
add(substract(...), divide(...))
```

---

# 💻 Calculator: Shape

---

## Requirements

Build a simple calculator to compute basic calculation of shape's area size, perimeter/circumference, or volume size.

### Input

* Numbers

### Process

* Several functions to run different kind of operations

### Output

* Calculated numbers

---

## Level 13

* Prepare all basic shape calculation you already learnt

---

## Level 14

* Wrap those operations into different kind functions based on the shapes

```js
function calculateSquareArea(...) { ... }
function calculateSquarePerimeter(...) { ... }
function calculateCircleArea(...) { ... }
function calculateCircleCircumference(...) { ... }
function calculateCubeArea(...) { ... }
function calculateCubeVolume(...) { ... }
function calculateTubeArea(...) { ... }
function calculateTubeVolume(...) { ... }
// and much more
```

---

## Level 15

* Experiment to create variation and call some chain of different functions you already made with these new functions

```js
calculateSquareArea(add(...))
calculateTubeVolumeWithCircle(calculateCircleArea())
```

---

# 💻 Calculator: Time

---

## Requirements

Build a simple calculator to calculate time.

### Input

* Time in numbers with measurement you specify
* Time with `Date` object

### Process

* Several functions to run different kind of operations

### Output

* Calculated time

---

## Level 16

* Prepare all basic time operations you already learnt with just utilizing number
* For example, determine how to get value of seconds, minutes, hours, days, months, years based on different time

---

## Level 18

* Prepare time operations with `Date` object
* Get different kind desired values through the built-in methods

Test Case:

```js

```

---

## Level 19

* Wrap those operations into different kind functions
* Learn which way is the best to calculate time dimension

---

## Level 23

* Experiment to create variation and call some chain of different functions in other functions

---

# 💻 Address Book

---

## Requirements

Let's create an address book simulation.

### Input

* People's contact information

### Process

* Storing and retrieving information

### Output

* Retrieved information

---

## Level 24

* Research a bit what you're going to model the address book after
* Sketch out your plan, try to type a simple pseudocode, then get some experiment going!
* Have fun of creating an address book simulation. :)

---

## Level 25

* Store and retrieve a single type of information in an array

---

## Level 26

* Store and retrieve multiple types of information in several objects that stored in an array

---

# 💻 Todo List

---

## Requirements

Let's create a simple todo list program.

### Input

* Our todo list

### Process

* Storing and retrieving information

### Output

* Our todo list ;)

---

## Level 27

* Research a bit what you're going to model the todo list after
* Sketch out your plan, try to type a simple pseudocode, then get some experiment going!
* Have fun of creating a todo list program. :)

---

## Level 28

* Store and retrieve a single type of information in an array

---

## Level 29

* Store and retrieve multiple types of information in several objects that stored in an array
